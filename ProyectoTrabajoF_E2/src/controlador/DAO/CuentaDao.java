/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.DAO;

import controlador.tda.lista.ListaEnlazadaServices;
import modelo.Login;

/**
 *
 * @author DELL
 */
public class CuentaDao extends AdaptadorDao<Login> {

    private Login cuenta;
    private ListaEnlazadaServices<Login> usuario;

    public CuentaDao() {
        super(Login.class);
    }

    public Login getCuenta() {
        if (cuenta == null) {
            cuenta = new Login();
        }
        return cuenta;
    }

    public void setCuenta(Login cuenta) {
        this.cuenta = cuenta;
    }

    public ListaEnlazadaServices<Login> getUsuario() {
        return usuario;
    }

    public void setUsuario(ListaEnlazadaServices<Login> usuario) {
        this.usuario = usuario;
    }

    public boolean guardar() {
        try {
            getCuenta().setId(usuario.getSize() + 1);
            guardar(getCuenta());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar usuario");
        }
        return false;
    }

    public ListaEnlazadaServices<Login> listado() {
        setUsuario(listar());
        return usuario;
    }

    public Login buscarCuentaUser(String dato) {
        Login c = null;
        ListaEnlazadaServices<Login> lista = listar();
        for (int i = 0; i < listar().getSize(); i++) {
            if (dato.toLowerCase().equals(lista.obtenerDato(i).getUser())) {
                c = lista.obtenerDato(i);
            }
        }
        return c;
    }

    public Login inicioSecion(String user, String password) {
        Login c = buscarCuentaUser(user);
        return (c != null && c.getPassword().equals(password)) ? c : null;
    }
    public ListaEnlazadaServices<Login> ordenar() {
        try {
            ListaEnlazadaServices<Login> lista = listar();
            int intercambio = 0;
            for (int i = 0; i < lista.getSize()- 1; i++) {
                int k = i;
                Login t = lista.obtenerDato(i);//datos[i];            
                for (int j = i + 1; j < lista.getSize(); j++) {
                    if (lista.obtenerDato(j).getUser().toLowerCase().compareTo(t.getUser().toLowerCase()) < 0) {
                        t = lista.obtenerDato(j);
                        k = j;
                        intercambio++;
                    }
                }
                lista.modificarDatoPosicion(k, lista.obtenerDato(i));
                lista.modificarDatoPosicion(i, t);
            }
            return lista;
        } catch (Exception e) {
            return new ListaEnlazadaServices<>();
        }
    }

}
